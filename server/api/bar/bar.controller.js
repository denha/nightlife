/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/bars              ->  index
 * POST    /api/bars              ->  create
 * GET     /api/bars/:id          ->  show
 * PUT     /api/bars/:id          ->  update
 * DELETE  /api/bars/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Bar from './bar.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function (entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function saveUser(updates) {
  return function (entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Bars
export function index(req, res) {
  return Bar.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Bar from the DB
export function show(req, res) {
  return Bar.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function find(req, res) {
  return Bar.find({bar:req.params.id}).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Bar in the DB
export function create(req, res) {
  return Bar.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Bar in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Bar.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Bar in the DB with a new attendee
export function updateUser(req, res) {
  Bar.findByIdAndUpdate(req.params.id,
    {$addToSet: {"going":  req.body.user }},
    {safe: true, upsert: true, new: true}, function (res2, model) {
    if (res2) {
      console.log(res2);
      return res.send(res2);
    }
    return res.json(model);
  });
}

// Updates an existing Bar in the DB, removes an attendee
export function deleteUser(req, res) {
  var bar = {bar: req.params.id, going: req.params.going};
  console.log(bar);
  console.log(bar.bar);
  console.log(bar.going);
  Bar.remove(bar, function(err) {
    console.log(err);
    res.send(err);
  });
}

// Deletes a Bar from the DB
export function destroy(req, res) {
  return Bar.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
