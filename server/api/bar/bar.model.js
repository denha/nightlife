'use strict';

import mongoose from 'mongoose';

var BarSchema = new mongoose.Schema({
  bar: String,
  going: String
});

export default mongoose.model('Bar', BarSchema);
