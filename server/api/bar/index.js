'use strict';

var express = require('express');
var controller = require('./bar.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/going/:id', controller.find);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.delete('/going/:id/:going', controller.deleteUser);

module.exports = router;
