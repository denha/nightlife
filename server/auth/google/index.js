'use strict';

import express from 'express';
import passport from 'passport';
import {setTokenCookie} from '../auth.service';

var router = express.Router();
router
  .get('/', function (req, res, next) {
    console.log(req.query.location);
    exports.loc = function () {
      return req.query.location;
    };
    passport.authenticate('google', {
      failureRedirect: '/signup',
      scope: [
        'profile',
        'email'
      ],
      session: false
    })(req, res, next);
  })
  .get('/callback', passport.authenticate('google', {
    failureRedirect: '/signup',
    session: false
  }), setTokenCookie);

export default router;
