'use strict';

angular.module('nightlifeApp.auth', [
  'nightlifeApp.constants',
  'nightlifeApp.util',
  'ngCookies',
  'ngRoute'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
