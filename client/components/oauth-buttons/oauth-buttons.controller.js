'use strict';

angular.module('nightlifeApp')
  .controller('OauthButtonsCtrl', function($window, $rootScope) {
    this.loginOauth = function(provider) {
      $window.location.href = '/auth/' + provider + '?location=' + $rootScope.prev();
    };
  });
