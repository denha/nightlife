'use strict';

angular.module('nightlifeApp', [
    'nightlifeApp.auth',
    'nightlifeApp.admin',
    'nightlifeApp.constants',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'btford.socket-io',
    'ui.bootstrap',
    'validation.match'
  ])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
  })
  .run(function ($rootScope, $location) {

    var history = [];

    $rootScope.$on('$routeChangeSuccess', function () {
      history.push($location.$$path);
      console.log(history);
    });

    $rootScope.back = function () {
      $location.path($rootScope.prev());
    };

    $rootScope.prev = function() {
      return history.length > 1 ? history.splice(-2)[0] : "/";
    }

  });


