'use strict';
(function () {

  class MainController {

    constructor($http, $scope, $routeParams, Auth) {
      $scope.isLoggedIn = Auth.isLoggedIn;
      Auth.getCurrentUser(function(user) {
        $scope.currentUser = user;
      });
      Auth.search = $scope.location = $routeParams.location;
      this.$http = $http;
      this.bars = [];

      if (typeof $scope.location !== 'undefined') {
        $http.get('/api/yelp', {params: {location: $scope.location}}).then((response) => {
          console.log(response);
          this.bars = response.data.businesses;
        });
      }
    }


  }

  angular.module('nightlifeApp').controller('MainController', MainController);

})();
