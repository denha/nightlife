'use strict';

describe('Directive: attend', function () {

  // load the directive's module and view
  beforeEach(module('nightlifeApp'));
  beforeEach(module('app/main/attend/attend.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<attend></attend>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the attend directive');
  }));
});
