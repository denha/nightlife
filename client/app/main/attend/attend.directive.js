'use strict';

angular.module('nightlifeApp')
  .directive('attend', function ($http, $location, Auth) {
    return {
      templateUrl: 'app/main/attend/attend.html',
      restrict: 'E',
      scope: {},
      link: function (scope, element, attrs) {
        scope.going = false;
        scope.text = "I'm going";
        scope.btnClass = "btn-success";
        scope.bar_id = attrs.bar;

        refreshGoing(true);
        function refreshGoing(set) {
          $http.get('/api/bars/going/' + scope.bar_id).then(function success(response) {
            console.log(response);
            scope.attending = response.data;
            if (set) {
              Auth.getCurrentUser(function (user) {
                scope.going = !scope.attending.every(function (value) {
                  return (value.going !== user.email)
                });
              });
            }

          });
        }

        scope.clicked = function () {
          if (Auth.isLoggedIn()) {
            scope.setGoing();
          } else {
            $location.url('/login');
          }
          console.log(`clicked on ${scope.bar_id}`)
        };

        scope.setGoing = function () {
          if (scope.going) { // was not going, now going
            scope.text = "Not going anymore";
            scope.btnClass = "btn-danger";

            Auth.getCurrentUser(function (user) {
              $http.post('/api/bars/', {
                bar: scope.bar_id,
                going: user.email
              }).then(function success(response) {
                console.log(response);
                refreshGoing();
              }, function error(response) {
                console.log(response);
              });
            });

          } else {
            scope.text = "I'm going";
            scope.btnClass = "btn-success";
            Auth.getCurrentUser(function (user) {
              $http.delete('/api/bars/going/' + scope.bar_id + '/' + user.email).then(function success(response) {
                console.log(response);
                scope.bar = response.data;
                refreshGoing();
              }, function error(response) {
                console.log(response);
              });
            });
          }
        }
      }
    };
  });
