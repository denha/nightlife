'use strict';

angular.module('nightlifeApp')
  .config(function($routeProvider) {
    $routeProvider
      .when('/go/back', {
        template: "<div></div>",
        controller: function($rootScope) {
          $rootScope.back();
        }
      })
      .when('/:location', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: '$ctrl'
      })
      .when('/', {
        templateUrl: 'app/search/search.html',
        controller: 'SearchController'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
