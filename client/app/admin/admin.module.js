'use strict';

angular.module('nightlifeApp.admin', [
  'nightlifeApp.auth',
  'ngRoute'
]);
