'use strict';

angular.module('nightlifeApp')
  .controller('SearchController', function ($scope, $location, Auth) {

    console.log(`was ${$scope.location}`);
    Auth.getCurrentUser(function (user) {
      if (user) {
        console.log(`now ${user.location}`);
        $scope.location = user.location;
      }
    });
    $scope.submit = function (location) {
      $scope.location = location;
      console.log(location);
      Auth.changeLocation(location)
        .then(() => {
          console.log('changed.');
        })
        .catch(() => {
          console.log('not changed.');
        });
      $location.url(location);
    }
  });
