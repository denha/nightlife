'use strict';

class LoginController {
  constructor(Auth, $location, $rootScope) {
    this.user = {};
    this.errors = {};
    this.submitted = false;

    this.Auth = Auth;
    this.$location = $location;
    this.$rootScope = $rootScope;
  }

  login(form) {
    this.submitted = true;

    if (form.$valid) {
      this.Auth.login({
          email: this.user.email,
          password: this.user.password
        })
        .then(() => {
          // Logged in, redirect to home
          this.Auth.getCurrentUser((user) => {
            this.$rootScope.back();
          });
        })
        .catch(err => {
          this.errors.other = err.message;
        });
    }
  }
}

angular.module('nightlifeApp')
  .controller('LoginController', LoginController);
